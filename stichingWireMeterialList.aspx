﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="stichingWireMeterialList.aspx.cs" Inherits="stichingWire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <main id="main" class="main">

    <div class="pagetitle">
      <h1>Stiching Wire Product List </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Add Product</li>
          <li class="breadcrumb-item active">Stiching Wire Product list</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


          <section class="section">
        <div class="row">
            
           
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Get Stiching Wire Product List</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-5">
                                <lable>Wire Type</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="dropWireType">
                                      <asp:ListItem>Stiching Wire</asp:ListItem>
                                      
                                    
                                 </asp:DropDownList>
                            </div>
                            
                             <div class="col-md-6">
                                <lable>Company Name</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="dropCompanyName">
                                      <asp:ListItem>---Select---</asp:ListItem>
                                      
                                    
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-success" Text="Search" style="float:left;"/>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>


    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Product List</h5>
              
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">Wire Type</th>
                   <th scope="col">Waight</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Stiching Wire</td>
                    <td>10</td>
                    <td><asp:Button ID="btnView" runat="server" CssClass="btn btn-primary" Text="View" style="float:right;"/></td>
                   
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
</asp:Content>

