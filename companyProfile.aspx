﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="companyProfile.aspx.cs" Inherits="companyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">

    <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Celler & Buyer</li>
          <li class="breadcrumb-item active">Company Profile</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section profile">
      <div class="row">
         <div class="col-xl-6">

          <div class="card p-3">
            <div class="card-body pt-3">
              <!-- Bordered Tabs -->
              
              <div class="tab-content pt-2">

                <div class="tab-pane fade show active profile-overview" id="profile-overview">
                 
                  <p class="small fst-italic"></p>

                  <h5 class="card-title">Company Details</h5>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label ">Full Name</div>
                    <div class="col-lg-9 col-md-8">abc</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Company</div>
                    <div class="col-lg-9 col-md-8">xyz</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Company Type</div>
                    <div class="col-lg-9 col-md-8">Celler/Buyer</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Country</div>
                    <div class="col-lg-9 col-md-8">USA</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Address</div>
                    <div class="col-lg-9 col-md-8">P. No. 7 Gyan Vihar Coolony BK Kaul Nager Ajmer</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Phone</div>
                    <div class="col-lg-9 col-md-8">91-435534533</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Email</div>
                    <div class="col-lg-9 col-md-8">demo@gmail.com</div>
                  </div>

                </div>

               
              </div><!-- End Bordered Tabs -->

            </div>
          </div>

        </div>
         <div class="col-xl-6">

          <div class="card p-3">
            <div class="card-body pt-3">
              <!-- Bordered Tabs -->
              
              <div class="tab-content pt-2">

                <div class="tab-pane fade show active profile-overview" id="profile-overview">
                 
                  <p class="small fst-italic"></p>

                  <h5 class="card-title">Company Bank Details</h5>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label ">Bank Name</div>
                    <div class="col-lg-9 col-md-8">Bank Name</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Bank Account No</div>
                    <div class="col-lg-9 col-md-8">xyz</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">Bank Ifsc Code</div>
                    <div class="col-lg-9 col-md-8">abc123</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">GST No</div>
                    <div class="col-lg-9 col-md-8">ABC123</div>
                  </div>

                  <div class="row">
                    <div class="col-lg-3 col-md-4 label">UPI Id</div>
                    <div class="col-lg-9 col-md-8">31265
                    </div>
                  </div>

                  
                </div>

               
              </div><!-- End Bordered Tabs -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
    <!-- End #main -->
</asp:Content>

