﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="staffList.aspx.cs" Inherits="staffList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">
        <div class="pagetitle">
      <h1>Staff List</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Staff Detail</li>
          <li class="breadcrumb-item active">Staff List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
        <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card p-3">
            <div class="card-body">
              <h5 class="card-title">Staff List</h5>
                              <div class="row g-3">
                            <div class="col-md-3">
                               
                            </div>
                            
                             <div class="col-md-4">
                                <lable>Designation</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="dropCompanyName">
                                      <asp:ListItem>---Select---</asp:ListItem>
                                      <asp:ListItem>Manager</asp:ListItem>
                                      <asp:ListItem>Telecaller</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-2 mt-5">
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-success" Text="Search" />
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </div>
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Staff Name</th>
                   <th scope="col">DOJ</th>
                      <th scope="col">Designation</th>
                      <th scope="col">Contact No.</th>
                       <th scope="col"></th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>abc....</td>
                    <td>10</td>
                      <td>13121564</td>
                      <td>13121564</td>
                    <td><asp:Button ID="btnView" runat="server" CssClass="btn btn-primary" Text="View" style="float:right; margin:2px;"/>
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-warning" Text="Edit" style="float:right; margin:2px;"/>
                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-danger" Text="Delete" style="float:right; margin:2px;"/>
                    </td>
                   
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>
    </main><!-- End #main -->
</asp:Content>

