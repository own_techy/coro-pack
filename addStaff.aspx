﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addStaff.aspx.cs" Inherits="addStaff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <main id="main" class="main">
       <div class="pagetitle">
      <h1>Staff</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Staff Detail</li>
          <li class="breadcrumb-item active">Add Staff</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Add Staff</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">


                            <div class="col-md-6">
                                
                                <asp:FileUpload runat="server" ID="fileUpload" />
                               
                            </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Staff Name</lable>
                                <asp:TextBox runat="server" ID="companyName" CssClass="form-control mt-2" placeholder="Enter Staff Name"></asp:TextBox>
                            </div>
                            
                            <div class="col-md-6">
                                <lable>Father's Name</lable>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control mt-2" placeholder="Enter Father's Name"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Contact No.</lable>
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control mt-2" placeholder="Enter Contact No"></asp:TextBox>
                             </div>
                             <div class="col-md-6">
                                <lable>Whatsapp No</lable>
                                <asp:TextBox runat="server" ID="TextBox6" CssClass="form-control mt-2" placeholder="Enter Contact No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Designation</lable>
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control mt-2" placeholder="Enter Designation"></asp:TextBox>
                             </div>
                             <div class="col-md-6">
                                <lable>Date Of Birth</lable>
                                <asp:TextBox runat="server" ID="txtBankName" CssClass="form-control mt-2" placeholder="DD//MM//YYYY"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Date Of Joining</lable>
                                <asp:TextBox runat="server" ID="TextBox4" CssClass="form-control mt-2" placeholder="DD//MM//YYYY"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                 <lable><strong style="color:red;">*</strong>ID Proof</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCountry">
                                            <asp:ListItem>---Select---</asp:ListItem>
                                            <asp:ListItem>Aadhar Card No</asp:ListItem>
                                             <asp:ListItem>Pan Card No</asp:ListItem>
                                             <asp:ListItem>Votter ID Card</asp:ListItem>
                                             <asp:ListItem>Other</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Document Detail</lable>
                                <asp:TextBox runat="server" ID="TextBox5" CssClass="form-control mt-2" placeholder="Enter Document Detail"></asp:TextBox>
                             </div>
                            <div class="col-md-12">
                                <lable><strong style="color:red;">*</strong>Full Address</lable>
                                <asp:TextBox TextMode="MultiLine" Rows="2" runat="server" CssClass="form-control mt-2" ID="txtArea" placeholder="Enter Full Address"></asp:TextBox>
                                   
                             </div>
                            
                            <div >
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback" >
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           <div>
                               <asp:Button ID="registractionSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" style="float:right;"/>
                               
                            </div>
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
</main>
</asp:Content>

