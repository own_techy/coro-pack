﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addCompany.aspx.cs" Inherits="addCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">
       <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Setting</li>
          <li class="breadcrumb-item active">Add Company</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Compny Detail</h5>

                        <!-- No Labels Form -->
                        <div class="row g-3">
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Company Name</lable>
                                <asp:TextBox runat="server" ID="companyName" CssClass="form-control mt-2" placeholder="Enter Your Company Name"></asp:TextBox>
                            </div>
                            
                            <div class="col-md-6">
                                <lable>Email</lable>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control mt-2" placeholder="Enter Your Email"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable>Whatsapp No.</lable>
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control mt-2" placeholder="Enter Your Whatsapp No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Contact No</lable>
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control mt-2" placeholder="Enter Your Contact No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                 <lable><strong style="color:red;">*</strong>Comany Type</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="dropCompanyType">
                                            <asp:ListItem>---Select---</asp:ListItem>
                                            <asp:ListItem>Buyer</asp:ListItem>
                                            <asp:ListItem>Celler</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                 <lable><strong style="color:red;">*</strong>Celler Type</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList2">
                                            <asp:ListItem>---Select---</asp:ListItem>
                                            <asp:ListItem>Paper</asp:ListItem>
                                            <asp:ListItem>Starch</asp:ListItem>
                                            <asp:ListItem>Stiching Wire</asp:ListItem>
                                            <asp:ListItem>Ink</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                             <div class="col-md-12">
                                <lable><strong style="color:red;">*</strong>Street & Landmark</lable>
                                <asp:TextBox runat="server" ID="TextBox4" CssClass="form-control mt-2" placeholder="Street & Landmark"></asp:TextBox>
                             </div>
                             <div class="col-md-4">
                                  <lable><strong style="color:red;">*</strong>City</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCity">
                                            <asp:ListItem>---Select City---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>State</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectState">
                                            <asp:ListItem>---Select State---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>Country</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCountry">
                                            <asp:ListItem>---Select Country---</asp:ListItem>
                                            <asp:ListItem>India</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </Div>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
    </section>
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Bank Detail</h5>

                        <!-- No Labels Form -->
                        <div class="row g-3">
                            
                             <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Bank Name</lable>
                                <asp:TextBox runat="server" ID="TextBox9" CssClass="form-control mt-2" placeholder="Enter Bank Name"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Bank Account No.</lable>
                                <asp:TextBox runat="server" ID="TextBox10" CssClass="form-control mt-2" placeholder="Enter Account No"></asp:TextBox>
                             </div>
                            <div class="col-md-4">
                                <lable><strong style="color:red;">*</strong>Bank Ifsc Code</lable>
                                <asp:TextBox runat="server" ID="TextBox11" CssClass="form-control mt-2" placeholder="Enter Bank Ifsc Code "></asp:TextBox>
                             </div>
                           <div class="col-md-4">
                                <lable><strong style="color:red;">*</strong>GST No</lable>
                                <asp:TextBox runat="server" ID="TextBox14" CssClass="form-control mt-2" placeholder="Enter GST No."></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <lable>UPI Id</lable>
                                <asp:TextBox runat="server" ID="TextBox5" CssClass="form-control mt-2" placeholder="Enter UPI Id"></asp:TextBox>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="Panel1" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           <div class="text-center" >
                               <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Submit" style="float:right;"/>
                               
                            </div>
                        </div>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
</main>
</asp:Content>

