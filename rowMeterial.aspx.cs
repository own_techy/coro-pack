﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class rowMeterial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void txtMeterialType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(txtMeterialType.Text=="Paper")
        {
            paperPanel.Visible = true;
            inkPanel.Visible = false;
            starchPanel.Visible = false;
            stachingWirePanel.Visible = false;
        }
        else if(txtMeterialType.Text=="Ink")
        {
            paperPanel.Visible = false;
            inkPanel.Visible = true;
            starchPanel.Visible = false;
            stachingWirePanel.Visible = false;
        }
        else if (txtMeterialType.Text == "Starch")
        {
            paperPanel.Visible = false;
            inkPanel.Visible = false;
            starchPanel.Visible = true;
            stachingWirePanel.Visible = false;
        }
        else if (txtMeterialType.Text == "Staching Wire")
        {
            paperPanel.Visible = false;
            inkPanel.Visible = false;
            starchPanel.Visible = false;
            stachingWirePanel.Visible = true;
        }
    }
}