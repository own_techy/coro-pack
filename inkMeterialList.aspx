﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="inkMeterialList.aspx.cs" Inherits="inkMeterial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <main id="main" class="main">

    <div class="pagetitle">
      <h1>Ink Product List </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Row Stock</li>
          <li class="breadcrumb-item active">Ink Product List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


          <section class="section">
        <div class="row">
            

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Get Ink Product List</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-4">
                                <lable>Ink Color</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtBf">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Black</asp:ListItem>
                                      <asp:ListItem>Blue</asp:ListItem>
                                     <asp:ListItem>Green</asp:ListItem>
                                     <asp:ListItem>red</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            
                            <div class="col-md-7">
                                <lable>Company</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList1">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                     
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Search" style="float:right;"/>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>


    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Product List</h5>
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">Ink Color</th>
                   <th scope="col">Waight</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Red Ink</td>
                    <td>1</td>
                    <td><asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" Text="View" style="float:right;"/></td>
                   
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
</asp:Content>

