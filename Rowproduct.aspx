﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Rowproduct.aspx.cs" Inherits="Rowproduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <main id="main" class="main">

    <div class="pagetitle">
      <h1>Product List </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Add Product</li>
          <li class="breadcrumb-item active">Paper Row Product</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

     <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Paper Row Meterial Product</h5>
                 </div>
                </div>
             </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Creat New Paper Product</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-2">
                                <lable></strong>Reel No.</lable>
                                <asp:TextBox runat="server" ID="txtReelNo" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable></strong>Reel Size</lable>
                                <asp:TextBox runat="server" ID="txtReelSize" CssClass="form-control mt-2" placeholder="In Inches"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Reel Waight</lable>
                                <asp:TextBox runat="server" ID="txtProductWaight" CssClass="form-control mt-2" placeholder="In Kg"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Paper Type</lable>
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtPaperType">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>VK</asp:ListItem>
                                    <asp:ListItem>RJ</asp:ListItem>
                                    <asp:ListItem>Semi</asp:ListItem>
                                    <asp:ListItem>Netural</asp:ListItem>
                                    <asp:ListItem>Duplex</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <lable>BF</lable>
                                
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtBf">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>14</asp:ListItem>
                                    <asp:ListItem>16</asp:ListItem>
                                    <asp:ListItem>18</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>22</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <lable>GSM</lable>
                               
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtGsm">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>80</asp:ListItem>
                                    <asp:ListItem>90</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>120</asp:ListItem>
                                    <asp:ListItem>140</asp:ListItem>
                                    <asp:ListItem>220</asp:ListItem>
                                    <asp:ListItem>240</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                             <div class="col-md-3">
                                <lable>Comp. RN</lable>
                                <asp:TextBox runat="server" ID="txtRn" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                             <div class="col-md-4">
                                <lable>Product Company</lable>
                                <asp:TextBox runat="server" ID="txtProductCompany" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <lable>State</lable>
                               
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList1">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Rajasthan</asp:ListItem>
                                    <asp:ListItem>Utter Pradesh</asp:ListItem>
                                    <asp:ListItem>Gujrat</asp:ListItem>
                                    
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-2 mt-5" >
                                 <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Add" /> 
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Paper Product List</h5>
              <p>Add lightweight datatables to your project with using the Simple DataTables library. Just add datatable class name to any table you wish to conver to a datatable</p>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">Reel No.</th>
                    <th scope="col">Reel Size</th>
                    <th scope="col">Reelwt</th>
                    <th scope="col">Paper Type</th>
                    <th scope="col">BF</th>
                    <th scope="col">GSM</th>
                      <th scope="col">Comp. RN</th>
                    <th scope="col">Product Company</th>
                      <th scope="col">State</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                      <td>Mahadev</td>
                      <td>rajasthan</td>
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
</asp:Content>

