﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="productList.aspx.cs" Inherits="productList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <main id="main" class="main">

    <div class="pagetitle">
      <h1>Product List </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Sales Department</li>
          <li class="breadcrumb-item active">Product List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Product List</h5>
              <p>Add lightweight datatables to your project with using the Simple DataTables library. Just add datatable class name to any table you wish to conver to a datatable</p>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Height</th>
                    <th scope="col">Width</th>
                    <th scope="col">Lenght</th>
                    <th scope="col">Rate</th>
                    <th scope="col">Wait</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Box</td>
                    <td>4.2Feet</td>
                    <td>4.2Feet</td>
                    <td>4.2Feet</td>
                     <td>150</td>
                    <td>.05</td>
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->

</asp:Content>

