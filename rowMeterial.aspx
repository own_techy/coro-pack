﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="rowMeterial.aspx.cs" Inherits="rowMeterial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">

    <div class="pagetitle">
      <h1>Starch Product List </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Add Row Stock</li>
          <li class="breadcrumb-item active">Row Meterial</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


          <section class="section">
        <div class="row">
            

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Add Row Meterial</h5>

                        <!-- No Labels Form -->
                        <div class="row g-3">
                            <div class="col-md-4">
                                <label>Meterial Type</label>
                                 <asp:DropDownList runat="server" OnSelectedIndexChanged="txtMeterialType_SelectedIndexChanged" AutoPostBack="true" CssClass="form-select mt-2" ID="txtMeterialType">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Paper</asp:ListItem>
                                      <asp:ListItem>Ink</asp:ListItem>
                                      <asp:ListItem>Starch</asp:ListItem>
                                      <asp:ListItem>Staching Wire</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            
                            <div class="col-md-8">
                                <label>Company</label>
                               <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtCompany">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      
                                 </asp:DropDownList>
                            </div>
                           <div class="col-md-6">
                                <label>Invoice No</label>
                                <asp:TextBox runat="server" ID="txtLabel" CssClass="form-control mt-2" placeholder="Enter Invoice No."></asp:TextBox>
                            </div>
                            <div class="col-md-5">
                                <label>Invoice Date</label>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control mt-2" placeholder="Invoice Date"></asp:TextBox>
                            </div>
                            <div class="col-md-1 mt-5" >
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Add" style="float:right;" />
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </div>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>



         



     <div class="card p-3">
            <div class="card-body">
               <h5 class="card-title">Add Row Meterial</h5>
                <!-- Paper Row Meterial -->
                <asp:Panel runat="server" ID="paperPanel" Visible="false">
                     <div class="p-2">
                        <div class="row g-3 m-2">
                            <div class="col-md-4">
                                <label></strong>Reel No.</label>
                                <asp:TextBox runat="server" ID="txtReelNo" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label></strong>Reel Size</label>
                                <asp:TextBox runat="server" ID="txtReelSize" CssClass="form-control mt-2" placeholder="In Inches"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label>Comp. RN</label>
                                <asp:TextBox runat="server" ID="txtRn" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <label>Reel Waight</label>
                                <asp:TextBox runat="server" ID="txtProductWaight" CssClass="form-control mt-2" placeholder="In Kg"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Paper Type</label>
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtPaperType">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>VK</asp:ListItem>
                                    <asp:ListItem>RJ</asp:ListItem>
                                    <asp:ListItem>Semi</asp:ListItem>
                                    <asp:ListItem>Netural</asp:ListItem>
                                    <asp:ListItem>Duplex</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>BF</label>
                                
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtBf">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>14</asp:ListItem>
                                    <asp:ListItem>16</asp:ListItem>
                                    <asp:ListItem>18</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>22</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>GSM</label>
                               
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtGsm">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>80</asp:ListItem>
                                    <asp:ListItem>90</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>120</asp:ListItem>
                                    <asp:ListItem>140</asp:ListItem>
                                    <asp:ListItem>220</asp:ListItem>
                                    <asp:ListItem>240</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                             
                             <div class="col-md-1 mt-5" >
                                 <asp:Button ID="Button2" runat="server" CssClass="btn btn-success" Text="Add" style="float:right;"/> 
                            </div>
                            </div>
                        </div>
                     <table class="table table-sm p-5">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Reel No.</th>
                    <th scope="col">Reel Size</th>
                    <th scope="col">Reelwt</th>
                    <th scope="col">Paper Type</th>
                    <th scope="col">BF</th>
                    <th scope="col">GSM</th>
                    <th scope="col">Comp. RN</th>
                    
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                     
                  </tr>
                  <tr>
                     <th scope="row">2</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                      
                  </tr>
                  <tr>
                     <th scope="row">3</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                     
                  </tr>
                 
                </tbody>
              </table>
                </asp:Panel>
                 <!-- close Paper Row Meterial -->
             </div>
                <!-- Starch Row Meterial -->
         <asp:Panel runat="server" ID="starchPanel" Visible="false">
                <div class="p-2">
                       <div class="row g-3">
                            <div class="col-md-4">
                                <label>Starch Type</label>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList2">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Corogation</asp:ListItem>
                                    <asp:ListItem>Pasting</asp:ListItem>
                                    
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <label>Quantity</label>
                                <asp:TextBox runat="server" ID="txtQuantity" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Total Waight</label>
                                <asp:TextBox runat="server" ID="txtWaight" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="Button3" runat="server" CssClass="btn btn-success" Text="Add" style="float:right;"/>
                            </div>
                            <div>
                                
                            </div>
                           
                        </div>
                        <table class="table table-sm p-5">
                <thead>
                  <tr>
                     <th scope="col">S.No.</th>
                    <th scope="col">Starch Type</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Total Waight</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <th scope="row">1</th>
                    <td>Corogation</td>
                    <td>10</td>
                    <td>125</td>
                  </tr>
                  <tr>
                      <th scope="row">2</th>
                    <td>Corogation</td>
                    <td>10</td>
                    <td>125</td>
                  </tr>
                  <tr>
                      <th scope="row">3</th>
                    <td>Corogation</td>
                    <td>10</td>
                    <td>125</td>
                  </tr>
                  <tr>
                    <th scope="row">1=4</th>
                    <td>Corogation</td>
                    <td>10</td>
                    <td>125</td>
                    
                  </tr>
                  
                </tbody>
              </table>
                   </div>
             </asp:Panel>
                <!-- Close Starch Row Meterial -->
          <asp:Panel runat="server" ID="stachingWirePanel" Visible="false">

                <!-- Wire Row Meterial -->
                    <div class="m-2">
                        <div class="row g-3">
                            <div class="col-md-6">
                                <label>Wire Type</label>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList3">
                                      <asp:ListItem>Stiching Wire</asp:ListItem>
                                      
                                    
                                 </asp:DropDownList>
                            </div>
                            
                            <div class="col-md-5">
                                <label>Waight</label>
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control mt-2" placeholder="In Kg"></asp:TextBox>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="Button4" runat="server" CssClass="btn btn-success" Text="Add" style="float:right;"/>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="Panel3" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </div>
                        <table class="table table-sm p-5">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Wire Type</th>
                   <th scope="col">Waight</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Stiching Wire</td>
                    <td>10</td>
                  </tr>
                  <tr>
                   <th scope="row">2</th>
                    <td>Stiching Wire</td>
                    <td>10</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Stiching Wire</td>
                    <td>10</td>
                  </tr>
                  
                </tbody>
              </table>
                    </div>   
                <!-- Close Wite Row Meterial -->
            </asp:Panel>
          <asp:Panel runat="server" ID="inkPanel" Visible="false">

                 <!-- Ink Row Meterial -->
                 <form class="row g-3 p-3">
                            <div class="col-md-6">
                                <label>Ink Color</label>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="DropDownList4">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Black</asp:ListItem>
                                      <asp:ListItem>Blue</asp:ListItem>
                                     <asp:ListItem>Green</asp:ListItem>
                                     <asp:ListItem>red</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            
                            <div class="col-md-5">
                                <label>Waight</label>
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control mt-2" placeholder="In Litter"></asp:TextBox>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="Button5" runat="server" CssClass="btn btn-success" Text="Add" style="float:right;"/>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="Panel4" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </form>
                         <div class="">
                         
                            <table class="table table-sm p-5">
                                <thead>
                                  <tr>
                                   <th scope="col">#</th>
                                   <th scope="col">Ink Color</th>
                                   <th scope="col">Waight</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th scope="row">1</th>
                                     <td>Red Ink</td>
                                    <td>1</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">2</th>
                                    <td>Red Ink</td>
                                    <td>1</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">3</th>
                                    <td>Red Ink</td>
                                    <td>1</td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                        </div>
                 <!--Close Ink Row Meterial -->
              </asp:Panel>
            </div>

  </main><!-- End #main -->
</asp:Content>

