﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addMasterCategory.aspx.cs" Inherits="addMasterCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">

    <div class="pagetitle">
      <h1>Add Master Category </h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Setting</li>
          <li class="breadcrumb-item active">Master Category</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->


          <section class="section">
        <div class="row">
           

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-2 p-lg-5">
                        <h5 class="card-title">Add Master Category</h5>
                        
                        <!-- No Labels Form -->
                        <form class="row g-3">
                             <div class="col-md-3">
                                <lable>Label</lable>
                                <asp:TextBox runat="server" ID="txtLabel" CssClass="form-control mt-2" placeholder="label"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <lable>Value</lable>
                                <asp:TextBox runat="server" ID="txtValue" CssClass="form-control mt-2" placeholder="Value"></asp:TextBox>
                            </div>
                            <div class="col-md-2 mt-5">
                                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-success" Text="Add" />
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>


    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card p-2">
            <div class="card-body">
              <h5 class="card-title">Master Category</h5>
             
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Type</th>
                   <th scope="col">Label</th>
                    <th scope="col">Value</th>
                     <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Master Category</td>
                    <td>Stock</td>
                    <td>Stock</td> 
                     
                   <td><asp:Button ID="Button1" runat="server" CssClass="btn btn-danger m" Text="Delete" style="float:right;"/></td>
                  </tr>
                  <tr>
                    <th scope="row">1</th>
                    <td>Master Category</td>
                    <td>Stock</td>
                    <td>Stock</td> 
                     
                   <td><asp:Button ID="Button2" runat="server" CssClass="btn btn-danger" Text="Delete" style="float:right;"/></td>
                  </tr>
                </tbody>

              </table>
              <!-- End Table with stripped rows -->
                 
            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
</asp:Content>

