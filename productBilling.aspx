﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="productBilling.aspx.cs" Inherits="productBilling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <main id="main" class="main">
       <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Company Detail</li>
          <li class="breadcrumb-item active">Product Billing</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Creat a Bill</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Company Name</lable>
                                <asp:TextBox runat="server" ID="companyName" CssClass="form-control mt-2" placeholder="Enter Company Name"></asp:TextBox>
                            </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Custmer Name</lable>
                                <asp:TextBox runat="server" ID="custmerName" CssClass="form-control mt-2" placeholder="Enter Custmer Name"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Contact No(Company)</lable>
                                <asp:TextBox runat="server" ID="companyContact" CssClass="form-control mt-2" placeholder="Enter Contact No (Company)"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Contact No(Custmer)</lable>
                                <asp:TextBox runat="server" ID="custmerContact" CssClass="form-control mt-2" placeholder="Enter Contact No (Custmer)"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>GSTIN</lable>
                                <asp:TextBox runat="server" ID="txtGstin" CssClass="form-control mt-2" placeholder="Street & Landmark"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Address</lable>
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control mt-2" placeholder="Street & Landmark"></asp:TextBox>
                             </div>
                             <div class="col-md-4">
                                  <lable><strong style="color:red;">*</strong>City</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCity">
                                            <asp:ListItem>---Select City---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>State</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectState">
                                            <asp:ListItem>---Select State---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>Country</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCountry">
                                            <asp:ListItem>---Select Country---</asp:ListItem>
                                            <asp:ListItem>India</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Product Name</lable>
                                <asp:TextBox runat="server" ID="userProductName" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            <div class="col-md-3">
                                <lable><strong style="color:red;">*</strong>HSN Code</lable>
                                <asp:TextBox runat="server" ID="txthsnCode" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                             <div class="col-md-3">
                                <lable><strong style="color:red;">*</strong>Quantity</lable>
                                <asp:TextBox runat="server" ID="txtQuantity" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            <div class="col-md-2">
                                <lable><strong style="color:red;">*</strong>Rate</lable>
                                <asp:TextBox runat="server" ID="txtRate" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                             <div class="col-md-2">
                                <lable><strong style="color:red;">*</strong>Taxable Value</lable>
                                <asp:TextBox runat="server" ID="txtTotal" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            <div class="col-md-2">
                                <lable><strong style="color:red;">*</strong>CGST</lable>
                                <asp:TextBox runat="server" ID="txtCgst" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            <div class="col-md-2">
                                <lable><strong style="color:red;">*</strong>SGST</lable>
                                <asp:TextBox runat="server" ID="txtSgst" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            
                             <div class="col-md-2">
                                <lable><strong style="color:red;">*</strong>Total Amount</lable>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                             </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           <div class="text-center">
                               <asp:Button ID="registractionSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" />
                               
                            </div>
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
</main>
</asp:Content>

