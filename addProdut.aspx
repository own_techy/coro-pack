﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addProdut.aspx.cs" Inherits="addProdut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">
       <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Company Detail</li>
          <li class="breadcrumb-item active">Add Product</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Add Product</h5>
                 </div>
                </div>
             </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Creat New Product</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-2">
                                <lable></strong>Product Name</lable>
                                <asp:TextBox runat="server" ID="productName" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable></strong>Height</lable>
                                <asp:TextBox runat="server" ID="productHeight" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Width</lable>
                                <asp:TextBox runat="server" ID="productWidth" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Lenght</lable>
                                <asp:TextBox runat="server" ID="productLenght" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Rate</lable>
                                <asp:TextBox runat="server" ID="productRate" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <lable>Wait</lable>
                                <asp:TextBox runat="server" ID="productWait" CssClass="form-control mt-2" placeholder=""></asp:TextBox>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           <div class="text-center">
                               <asp:Button ID="productAdd" runat="server" CssClass="btn btn-primary" Text="Add" />
                               <asp:Button ID="productDelete" runat="server" CssClass="btn btn-danger" Text="Delete" />
                            </div>
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
</main>
</asp:Content>

