﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="paperMeterialList.aspx.cs" Inherits="rowMeterialList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">
       <div class="pagetitle">
          <h1>Paper Product List </h1>
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
              <li class="breadcrumb-item">Row Stock</li>
              <li class="breadcrumb-item active">Paper Row Meterial List</li>
            </ol>
          </nav>
        </div><!-- End Page Title -->
        <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Get Paper Meterial List</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-4">
                                <lable>Meterial Type</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtMeterialType">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>Paper</asp:ListItem>
                                      <asp:ListItem>Ink</asp:ListItem>
                                     <asp:ListItem>Starch</asp:ListItem>
                                     <asp:ListItem>Staching Wire</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            
                            <div class="col-md-8">
                                <lable>Company</lable>
                               <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtCompany">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      
                                 </asp:DropDownList>
                            </div>
                           <div class="col-md-5">
                                <lable>Paper Type</lable>
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtPaperType">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>VK</asp:ListItem>
                                    <asp:ListItem>RJ</asp:ListItem>
                                    <asp:ListItem>Semi</asp:ListItem>
                                    <asp:ListItem>Netural</asp:ListItem>
                                    <asp:ListItem>Duplex</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <lable>BF</lable>
                                
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtBf">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>14</asp:ListItem>
                                    <asp:ListItem>16</asp:ListItem>
                                    <asp:ListItem>18</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>22</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <lable>GSM</lable>
                               
                                <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="txtGsm">
                                      <asp:ListItem>--Select--</asp:ListItem>
                                      <asp:ListItem>80</asp:ListItem>
                                    <asp:ListItem>90</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>120</asp:ListItem>
                                    <asp:ListItem>140</asp:ListItem>
                                    <asp:ListItem>220</asp:ListItem>
                                    <asp:ListItem>240</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-1 mt-5">
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-success" Text="Search" style="float:right;"/>
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            
                                            <asp:Button ID="btnSucsessfull" runat="server" CssClass="btn-close" Text="" />
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <asp:Button ID="Button1" runat="server" CssClass="btnUnsucsessfull" Text="Add" />
                                         </div>
                         </asp:Panel>
                            </div>
                           
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
        <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Paper Product List</h5>
             <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">Reel No.</th>
                    <th scope="col">Reel Size</th>
                    <th scope="col">Reelwt</th>
                    <th scope="col">Paper Type</th>
                    <th scope="col">BF</th>
                    <th scope="col">GSM</th>
                      <th scope="col">Comp. RN</th>
                    <th scope="col">Product Company</th>
                      <th scope="col">State</th>
                      <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                      <td>Mahadev</td>
                      <td>rajasthan</td>
                      <td><asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" Text="View" style="float:right;"/></td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>abs123</td>
                    <td>33</td>
                    <td>135</td>
                    <td>vk</td>
                     <td>150</td>
                    <td>231</td>
                      <td>SC-01</td>
                      <td>Mahadev</td>
                      <td>rajasthan</td>
                      <td><asp:Button ID="Button3" runat="server" CssClass="btn btn-primary" Text="View" style="float:right;"/></td>
                  </tr>
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>
    </main>
</asp:Content>

