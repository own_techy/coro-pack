﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="companyRegistraction.aspx.cs" Inherits="companyRegistraction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
   <main id="main" class="main">
       <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item">Company Detail</li>
          <li class="breadcrumb-item active">Company Registraction</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Company Registraction Form</h5>
                 </div>
                </div>
             </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body p-4 p-lg-5">
                        <h5 class="card-title">Fill The Form And Register Your Compny</h5>

                        <!-- No Labels Form -->
                        <form class="row g-3">
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Company Name</lable>
                                <asp:TextBox runat="server" ID="companyName" CssClass="form-control mt-2" placeholder="Enter Your Company Name"></asp:TextBox>
                            </div>
                            
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Email</lable>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control mt-2" placeholder="Enter Your Email"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable>Whatsapp No.</lable>
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control mt-2" placeholder="Enter Your Whatsapp No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Contact No</lable>
                                <asp:TextBox runat="server" ID="TextBox2" CssClass="form-control mt-2" placeholder="Enter Your Contact No"></asp:TextBox>
                             </div>
                             <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Bank Name</lable>
                                <asp:TextBox runat="server" ID="txtBankName" CssClass="form-control mt-2" placeholder="Enter Bank Name"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Bank Account No.</lable>
                                <asp:TextBox runat="server" ID="bankAccountNo" CssClass="form-control mt-2" placeholder="Enter Account No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Bank Ifsc Code</lable>
                                <asp:TextBox runat="server" ID="txtIfsc" CssClass="form-control mt-2" placeholder="Enter Bank Ifsc Code "></asp:TextBox>
                             </div>
                           <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Invoice No.</lable>
                                <asp:TextBox runat="server" ID="txtInvoice" CssClass="form-control mt-2" placeholder="Enter Invoice No"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Invoice Date</lable>
                                <asp:TextBox runat="server" ID="txtDate" CssClass="form-control mt-2" placeholder="DD//MM//YY"></asp:TextBox>
                             </div>
                            
                             <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>GST No</lable>
                                <asp:TextBox runat="server" ID="gstNo" CssClass="form-control mt-2" placeholder="Enter Your Company Name"></asp:TextBox>
                            </div>
                            <div class="col-md-12">
                                <lable><strong style="color:red;">*</strong>Street & Landmark</lable>
                                <asp:TextBox runat="server" ID="TextBox4" CssClass="form-control mt-2" placeholder="Street & Landmark"></asp:TextBox>
                             </div>
                             <div class="col-md-4">
                                  <lable><strong style="color:red;">*</strong>City</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCity">
                                            <asp:ListItem>---Select City---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>State</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectState">
                                            <asp:ListItem>---Select State---</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                 <lable><strong style="color:red;">*</strong>Country</lable>
                                        <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="selectCountry">
                                            <asp:ListItem>---Select Country---</asp:ListItem>
                                            <asp:ListItem>India</asp:ListItem>
                                        </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>User Name</lable>
                                <asp:TextBox runat="server" ID="userName" CssClass="form-control mt-2" placeholder="Enter A User Name"></asp:TextBox>
                             </div>
                            <div class="col-md-6">
                                <lable><strong style="color:red;">*</strong>Password</lable>
                                <asp:TextBox runat="server" ID="userpassword" CssClass="form-control mt-2" placeholder="Create A Password"></asp:TextBox>
                             </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           <div class="text-center">
                               <asp:Button ID="registractionSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" />
                               
                            </div>
                        </form>
                        <!-- End No Labels Form -->

                    </div>
                </div>


            </div>
        </div>
    </section>
</main>
</asp:Content>

