﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cellerBuyerList.aspx.cs" Inherits="cellerBuyerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <main id="main" class="main">
        <div class="pagetitle">
      <h1>Celler & Buyer</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="Default.aspx">Home</a></li>
          <li class="breadcrumb-item">Row Stock</li>
          <li class="breadcrumb-item active">Celler & Buyer List</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
        <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card p-3">
            <div class="card-body">
              <h5 class="card-title">Celler & Buyer List</h5>
                              <div class="row g-3">
                            <div class="col-md-3">
                               
                            </div>
                            
                             <div class="col-md-4">
                                <lable>Company Type</lable>
                                 <asp:DropDownList runat="server" CssClass="form-select mt-2" ID="dropCompanyType">
                                      <asp:ListItem>---Select---</asp:ListItem>
                                      <asp:ListItem>Celler</asp:ListItem>
                                      <asp:ListItem>Buyer</asp:ListItem>
                                 </asp:DropDownList>
                            </div>
                            <div class="col-md-2 mt-5">
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-success" Text="Search" />
                            </div>
                            <div>
                                <asp:Panel Visible="True" ID="passwordAlert" runat="server" CssClass="invalid-feedback">
                                

                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            Registraction Sucsessfull....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Fill Required Filds....
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                         </div>


                        </asp:Panel>
                            </div>
                           
                        </div>
              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Company Name</th>
                   <th scope="col">Address</th>
                      <th scope="col">Contact No.</th>
                       <th scope="col"></th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>abc....</td>
                    <td>10</td>
                      <td>13121564</td>
                    <td><a href="companyProfile.aspx"><asp:Button ID="btnView" runat="server" CssClass="btn btn-primary" Text="View" style="float:right;"/></a></td>
                   
                  </tr>
                  
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>
    </main><!-- End #main -->
</asp:Content>

